![](http://bonjourmonde.net/bonjourmonde/user/pages/12.Calendrier-des-revoltes-17/images/thumb.jpg)

# Révoltes

Processing program that "glitch" an image following a sound. Each pixel are moving in relation to the sound uploaded 

*Programme Processing de distorsion d'images, pensé par Bonjour Monde et écrit par Arman Mohtadji, à l'occasion d'une collaboration avec l'artiste Matthieu Saladin sur la troisième activation de son *Calendrier des Révoltes* (produit par le centre d'art Synesthésie).*

## Getting Started

This code is not suited for production, but it is not hard to use to get some nice result.  

### Prerequisites

You will need the **minim** library.   
 
Insert your files (sound and picture) into the ```data``` folder.   
Then adjust the numbers ```line 17 36 37 & 41``` in relation with your image size.   
Line ```45``` and ```54``` insert the name of your image and sound file.    
Line ```116``` change your output name to your taste.     

## Built With

Processing

## Author 

Bonjour Monde 

## License 

[GPL](https://www.gnu.org/licenses/gpl-3.0.en.html)




